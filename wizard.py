import streamlit as st
import inspect
import gettext
import rag

_ = gettext.gettext

st.title(_("Dataspace Wizard 🧙‍♂️"))

print("locales:", gettext.find('wizard', localedir='locales', all=True))

# el = gettext.translation('wizard', localedir='locales') #, languages=['el'])
# el.install()
# _ = el.gettext # Greek

class Choice():
    __answers = []
    __choices = {}

    def __init__(self) -> None:
        self.__choices[type(self).__name__] = None

    def _saveChoice(self, choice) -> None:
        self.__choices[type(self).__name__] = choice

    def ask(self) -> dict:
        choice = self._ask(question, answer)
        if choice:
            self._saveChoice(choice)
        return self.__choices
    
    def _saveAnswer(self, answer_context: str) -> None:
        self.__answers.append(answer_context)
        answer.write(answer_context)

    @classmethod
    def getAnswers(cls):
        return cls.__answers


class BooleanChoice(Choice):
    YES = _("Yes")
    NO = _("No")


class ChoiceWhoAreYou(Choice):
    # extracted from https://www.sitra.fi/en/publications/rulebook-for-a-fair-data-economy/#1-1-why-and-when-you-should-use-a-rulebook-for-data-sharing
    DATA_PROVIDER = _("Data Provider")
    SERVICE_PROVIDER = _("Service Provider")
    END_USER = _("End-User")
    INFRASTRUCTURE_OPERATOR = _("Infrastructure Operator")
    USER_TYPES = {
        DATA_PROVIDER: _("a source that provide the network with data."),
        SERVICE_PROVIDER: _("a data refiner that combine data streams, refine data, and provide them further. You provide services to End-Users or are a subcontractor to other Service Providers."),
        END_USER: _("an individual or organization for which a Service Provider has developed its services. You consume, utilize and access the value that is created in the data ecosystem."),
        INFRASTRUCTURE_OPERATOR: _("an actor providing identity management, consent management, logging, or service management services for the data network.")
    }

    def _ask(self, question, answer):
        choice = question.multiselect("Who are you ?", self.USER_TYPES.keys())
        for user_type in choice:
            self._saveAnswer(_("You are {user_type}".format(user_type=self.USER_TYPES[user_type])))
        return choice


class ChoiceBuildJoinUpdateDS(Choice):
    BUILD = _("Build a Dataspace")
    JOIN = _("Join a Dataspace")
    UPDATE = _("Update a Dataspace")

    def _ask(self, question, anwser):
        choice = question.radio(_("What do you want to do ?"), [self.BUILD, self.JOIN, self.UPDATE], index=None)

        if choice:
            self._saveAnswer(_("You want to {choice}.").format(choice=choice.lower()))
            self._saveChoice(choice)
        if choice == self.BUILD:
            ChoiceIsRegularedDomain().ask()
        elif choice in [self.JOIN, self.UPDATE]:
            ChoiceKnowYourDSAuthority().ask()


class ChoiceKnowYourDSAuthority(BooleanChoice):
    def _ask(self, question, answer):
        choice = question.radio(_("Do you know who is your Dataspace Authority ?"), [self.YES, self.NO], index=None)

        if choice == self.NO:
            self._saveAnswer(_("You don't know who is your Dataspace Authority."))
        elif choice == self.YES:
            self._saveAnswer(_("You know who is your Dataspace Authority."))
        return choice


class ChoiceIsRegularedDomain(BooleanChoice):
    def _ask(self, question, answer):
        choice = question.radio(_("Are you in a regulated domain ?"), [self.YES, self.NO], index=None)

        if choice == self.NO:
            self._saveAnswer(_("You don't have domain specific regulation."))
        elif choice == self.YES:
            self._saveAnswer(_("You have domain specific regulation."))
        return choice


class ChoiceHasDataExchangeService(BooleanChoice):
    def _ask(self, question, answer):
        choice = question.radio(_("Do you already have services ready for exchanging data ?"), [self.YES, self.NO], index=None)

        if choice == self.NO:
            self._saveAnswer(_("You need to select and deploy your software stack or contact a {service_provider}.".format(service_provider=ChoiceWhoAreYou.SERVICE_PROVIDER)))
        elif choice == self.YES:
            self._saveAnswer(_("You have the software and components to exchange data."))
        return choice


# def display_messages():
#     for i, (msg, is_user) in enumerate(st.session_state["messages"]):
#         message(msg, is_user=is_user, key=str(i))
#     st.session_state["thinking_spinner"] = st.empty()


def process_input():
    if st.session_state["user_input"] and len(st.session_state["user_input"].strip()) > 0:
        user_text = st.session_state["user_input"].strip()
        # with st.session_state["thinking_spinner"], st.spinner(f"Thinking"):
        #     if st.session_state["assistant"]:
        agent_text = st.session_state["assistant"].ask(user_text)
        prompt.write(agent_text)
        # st.session_state["messages"].append((user_text, True))
        # st.session_state["messages"].append((agent_text, False))


## no main, ugly but lazy and convenient for global variables.

# print(st.session_state)
# if len(st.session_state) == 0:
#     st.session_state["messages"] = []
#     st.session_state["assistant"] = None

# simple page layout with 4 sections
st.markdown("## Questions")
question = st.container()
st.markdown("---")
st.markdown("## Answers")
answer = st.container()
st.markdown("---")
st.markdown("## Prompt")
prompt = st.container()
#st.session_state["ingestion_spinner"] = st.empty()
st.markdown("---")
st.markdown("## Debug")
debug = st.container()


# start the wizard

choices = ChoiceWhoAreYou().ask()
if choices[ChoiceWhoAreYou.__name__]: # the question is answered
    choices = ChoiceBuildJoinUpdateDS().ask()

    if ChoiceWhoAreYou.DATA_PROVIDER in choices[ChoiceWhoAreYou.__name__]: # the anwser contains being a data provider
        choices = ChoiceHasDataExchangeService().ask()

if None in choices.values():
    prompt.write("You need to answer all questions before using the prompt.")
else:
    prompt.write("Let's go !")
    st.session_state["assistant"] = rag.WizardPrompt(Choice.getAnswers())
#    display_messages()
    prompt.text_input("Message", key="user_input", on_change=process_input)

debug.write(choices)
debug.write(Choice.getAnswers())

