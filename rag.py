#!/usr/bin/env python3

from bs4 import BeautifulSoup
# import chromadb
# import chromadb.utils.embedding_functions as embedding_functions
import colorlog
from langchain_community.chat_models import ChatOllama
from langchain_community.embeddings import FastEmbedEmbeddings
from langchain_community.vectorstores import Chroma
from langchain_core.document_loaders import BaseLoader
from langchain_core.documents import Document
from langchain_core.prompts import PromptTemplate
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_text_splitters import RecursiveCharacterTextSplitter
import logging
import os
from pathlib import Path
import sys


logger = colorlog.getLogger(__name__)
logger.addHandler(colorlog.StreamHandler())
logger.setLevel(level=logging.INFO)


class DSSCWebpageDocumentLoader(BaseLoader):
    """A loader that reads a DSSC webpage"""

    def __init__(self, filepath: Path) -> None:
        self.filepath = Path(filepath)

    def aload(self) -> Document:
        with open(self.filepath, encoding="utf-8") as fd:
            soup = BeautifulSoup(fd, "html.parser")
            printable_doc = soup.select('#printable_document')
            assert len(printable_doc), f"'{self.filepath}' has no element #printable_document"
            return Document(page_content=printable_doc[0].get_text(),
                            metadata={"title": soup.title.string, "source": self.filepath.name})

class DSSCBlueprintVectorsBuilder():
    def __init__(self, dirname: Path):
        self.docs_dirname = dirname.resolve()
        assert self.docs_dirname.is_dir(), f"'{self.docs_dirname}' is not a directory"
        self.db_filepath = Path(self.docs_dirname,  ".chroma_db")
        self.embedding_function = FastEmbedEmbeddings()
        # chroma_client = chromadb.PersistentClient(path="dssc.chromadb")
        # ollama_ef = embedding_functions.OllamaEmbeddingFunction(url="https://ollama.lab.gronlier.fr/api/embeddings",  model_name="llama3")
        # self.collection = chroma_client.get_or_create_collection(name="dssc") #, embedding_function=ollama_ef)
        # print(ollama_ef(["This is my first text to embed", "This is my second document"]))

# https://stackoverflow.com/a/78164483/1358422

    def createVectors(self):
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=1024, chunk_overlap=100)
        docs = []
        # assert sys.version_info >= (3, 12) # for Path.walk support
        for root, dirnames, filenames in self.docs_dirname.walk(on_error=print, follow_symlinks=False):
            for filename in filenames:
                if Path(root / filename).suffix not in ['.htm', '.html']:
                    continue
                logger.info(f"processing {filename}")
                loader = DSSCWebpageDocumentLoader(Path(root / filename))
                docs.append(loader.aload())
                # break
        logger.info(f"Found {len(docs)} document(s).")
        chunks = text_splitter.split_documents(docs)
        logger.info(f"Generated {len(chunks)} chunks.")
        self.vector_store = Chroma.from_documents(documents=chunks,
                                             embedding=self.embedding_function,
                                             persist_directory=self.db_filepath.as_posix())
        # for chunk in chunks:
        #     print(chunk)
        #     self.collection.add(ids=[hashlib.sha1(bytes(chunk.metadata['source'], encoding='raw_unicode_escape')).hexdigest()],
        #                         metadatas=[chunk.metadata], documents=[chunk.page_content])
        logger.info(f"There are {self.vector_store._collection.count()} in the collection.")

    def loadVectors(self, create_if_empty: bool = False):
        self.vector_store = Chroma(embedding_function=self.embedding_function,
                              persist_directory=self.db_filepath.as_posix())
        logger.info(f"There are {self.vector_store._collection.count()} in the collection.")
        if self.vector_store._collection.count() == 0 and create_if_empty:
            logger.info("The vector is empty. Loading documents.")
            self.createVectors()
        return self.vector_store

class WizardPrompt():
    def __init__(self, answers_context) -> None:
        print(answers_context)
        self.prompt = PromptTemplate.from_template(
            """
            <s> [INST] You are an assistant for question-answering tasks. Use the following pieces of retrieved context 
            to answer the question. If you don't know the answer, just say that you don't know. Use three sentences
             maximum and keep the answer concise. [/INST] </s> 
            [INST] Question: {question} 
            Context: {context} {answers_context}
            Answer: [/INST]
            """
        )
        print(self.prompt)
        self.prompt = self.prompt.partial(answers_context=" ".join(answers_context))
        print(self.prompt)

        vector_builder = DSSCBlueprintVectorsBuilder(Path('dssc'))
        vector_store = vector_builder.loadVectors(create_if_empty=True)

        self.retriever = vector_store.as_retriever(
            search_type="similarity_score_threshold",
            search_kwargs={"k": 3, "score_threshold": 0.5})

        self.model = ChatOllama(base_url="https://ollama.lab.gronlier.fr", model="phi3")
        
        self.chain = ({"context": self.retriever, "question": RunnablePassthrough()}
                      | self.prompt
                      | self.model
                      | StrOutputParser())

    def ask(self, query: str):
        return self.chain.invoke(query)


if __name__ == "__main__":
    logger.setLevel(level=logging.DEBUG)
    vector_builder = DSSCBlueprintVectorsBuilder(Path('dssc'))
    vector_builder.loadVectors(create_if_empty=True)
