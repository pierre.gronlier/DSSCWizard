# Dataspace Wizard 🧙‍♂️


## Run

```shell
streamlit run --logger.level info wizard.py
```


## Extra

## For development

```shell
docker run -it --rm --network host -v .:/data python:3.12.3-bookworm /bin/bash
cd /data
pip install -r requirements.txt
```

## To create vector store

```shell
./rag.py
```

### To extract messages for internationalisation

```shell
pygettext3 -d wizard wizard.py
```
